import React, { Component } from "react";
import Card from '../../components/Card/Card';
import './cardList.scss';
import Modal from '../../components/Modal/Modal'
import Button from '../../components/Button/Button'


class CardList extends Component {
    state = {
        cartGoods: [],
        favouriteGoods: [],
        modal: {
            id: '',
            name: '',
            showModal: false
        }

    }

    saveToLocalCart = (id, name) => {

        let arrCart = [];
        let localCart = localStorage.getItem("cart");

        this.saveToLocal(arrCart, localCart, id, name);
        this.closeModal();
    }

    saveToLocalFavour = (id, name) => {

        let arrFavour = [];
        let localFavour = localStorage.getItem("favourite");

        this.saveToLocal(arrFavour, localFavour, id, name);
    }

    saveToLocal = (array, localStor, id, name) => {
        if (localStor) {
            array = JSON.parse(localStor);

            if (array.includes(id)) {

                const index = array.findIndex((element) => {
                    return element === id;
                });
                array.splice(index, 1);

            } else {
                array.push(id);
            }

            array = Array.from(new Set(array));
            localStorage.setItem(name, JSON.stringify(array));

            if (name === "cart") {
                this.setState({
                    ...this.state,
                    cartGoods: array
                })
            }
            if (name === "favourite") {
                this.setState({
                    ...this.state,
                    favouriteGoods: array
                })
            }

        } else {
            array.push(id);
            localStorage.setItem(name, JSON.stringify(array));
        }
    }

    showModal = (id, name) => {

        this.setState({
            modal: {
                id: id,
                name: name,
                showModal: true
            }
        });
    }

    closeModal = () => {
        this.setState({
            modal: {
                ...this.state.modal,
                showModal: false
            }
        });
    }

    render() {
        const { goods } = this.props;
        const buttons = [
            <Button key='1' text={"OK"} className='btn-ok button' func={() => {
                this.saveToLocalCart(this.state.modal.id, this.state.modal.name);
            }} />,

            <Button key='2' text={"Cancel"} className="btn-cancel button" func={this.closeModal} />
        ];

        const modal = {
            title: "Add to card",
            content: "Do you wanna add the product to cart?",
            closeBurron: false
        }

        const cardsGoods = goods.map((good) => (
            <Card
                id={good.article}
                key={good.article}
                name={good.name}
                price={good.price}
                map={good.map}
                title={good.title}
                color={good.color}
                showModal={this.showModal}
                saveToLocalFavour={this.saveToLocalFavour}
                isFavourite={this.state.favouriteGoods.includes(good.article)} />
        ))

        return (
            <>
                <Modal
                    title={modal.title}
                    content={modal.content}
                    action={buttons}
                    modalIsOpen={this.state.modal.showModal}
                    closeModal={this.closeModal}
                />
                <div className='container d-flex justify-content-start flex-wrap cards-p' >
                    {cardsGoods}
                </div>
            </>
        );
    }
}


export default CardList;