import React, { Component } from 'react';
import './modal.scss';

class Modal extends Component {

    render() {
        const { title, content, action, modalIsOpen, closeModal } = this.props;
        const closeModalBtn = (<div className='close-btn' onClick={closeModal}>X</div>)

        const isCloseModal = () => {           
            closeModal();
        }

        return modalIsOpen && (

            <div className='shadow' onClick={isCloseModal}>
                <div className='window'>
                    <header className='modal-header'>
                        <div className="title">{title}</div>
                        <div>{closeModalBtn}</div>
                    </header>
                    <div className='modal-b'>{content}</div>
                    <div className='modal-controls'>{action}</div>
                </div>
            </div>
        )
    }
}

export default Modal;