import React, { Component } from "react";
import Button from "../Button/Button";
import './card.scss'
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";



class Card extends Component {


    render() {
        const { name, price, map, title, saveToLocalFavour, showModal, isFavourite } = this.props;

        const btn = {
            text: 'Add to catch',
            className: 'add-to-cart'
        }

        return (

            <div className="card-container">
                <img src={map} className="card-picture"></img>
                <h2 className="title-card">{name}
                    <span className="icon-star">
                        <FontAwesomeIcon
                            icon={faStar}
                            className={isFavourite ? "favourite-on" : "favourite-off"}
                            onClick={(e) => {
                                e.preventDefault();
                                saveToLocalFavour(this.props.id, "favourite");
                            }} />
                    </span>

                </h2>
                <p className="describe">{title}</p>

                <div className='footer'>
                    <span className="add-to-favour"></span>
                    <span className="price-card">Price: {price}$</span>
                    <Button text={btn.text} className={btn.className} func={(e) => {
                        e.preventDefault();
                        showModal(this.props.id, "cart");
                    }} />
                </div>
            </div>
        );
    }
}

export default Card;