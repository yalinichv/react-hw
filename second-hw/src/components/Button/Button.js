import React,{Component} from 'react';
import "./button.scss";


class Button extends Component {

    render() {
        const { text, func, className } = this.props;

        return (
            <button onClick={func} className={className}>
                {text}
            </button>
        )
    }
}

export default Button;