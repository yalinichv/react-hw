import React, { Component } from 'react';
import axios from 'axios';
import CardList from './containers/CardList/CardList'
import Header from './containers/Header/Header'
import './app.scss'

class App extends Component {

  state = {
    goods: []
  }

  componentDidMount() {
    console.log("AppDid")
    axios('/goods.json')
      .then(result => {

        this.setState({
          ...this.state,
          goods: result.data
        });
      })
  }

  render() {
    console.log("AppRender")

    return (
      <div className="App">

        <Header />
        <div className="container">
          <div className='row'>
            <CardList goods={this.state.goods} />
          </div>
        </div>


      </div>
    )
  }
}

export default App;
