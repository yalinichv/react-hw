import React, { useState, useEffect } from "react";
import { Route } from "react-router-dom";
import Card from '../../components/Card/Card';
import './cardList.scss';
import Modal from '../../components/Modal/Modal';
import Button from '../../components/Button/Button';
import Catch from '../../pages/Catch/Catch';
import Favourites from '../../pages/Favourites/Favourites';
import Header from '../Header/Header';


function CardList(props) {

    const [cartGoods, setCartGoods] = useState({ cartGoods: [] });
    const [favouriteGoods, setFavouriteGoods] = useState({ favouriteGoods: [] });
    const [modal, setModal] = useState({ id: '', name: '', showModal: false });
    const [switchModal, setSwitchModal] = useState({ switch: false });

    useEffect(() => {
        isSwitchModal(false);
    }, [])

    useEffect(() => {
        const getFavourite = localStorage.getItem("favourite");

        if (getFavourite) {
            let parsedFavourite = JSON.parse(getFavourite);
            setFavouriteGoods({ favouriteGoods: parsedFavourite });
        }

    }, []);

    useEffect(() => {
        const getCatch = localStorage.getItem("cart");

        if (getCatch) {
            let parsedCatch = JSON.parse(getCatch);
            setCartGoods({ cartGoods: parsedCatch });
        }

    }, []);


    const saveToLocalCart = (id, name) => {

        let arrCart = [];
        let localCart = localStorage.getItem("cart");

        saveToLocal(arrCart, localCart, id, name);
        closeModal();
    }

    const saveToLocalFavour = (id, name) => {

        let arrFavour = [];
        const localFavour = localStorage.getItem("favourite");
        saveToLocal(arrFavour, localFavour, id, name);
    }

    const saveToLocal = (array, localStor, id, name) => {

        if (localStor) {
            array = JSON.parse(localStor);

            if (array.includes(id) && name === 'favourite') {
                const index = array.findIndex((element) => {
                    return element === id;
                });
                array.splice(index, 1);
            } else {
                array.push(id);
            }

            // array = Array.from(new Set(array));
            localStorage.setItem(name, JSON.stringify(array));

            if (name === "cart") {
                setCartGoods({ cartGoods: array })
            }
            if (name === "favourite") {
                setFavouriteGoods({ favouriteGoods: array })
                console.log(favouriteGoods.favouriteGoods);
            }

        } else {
            array.push(id);
            localStorage.setItem(name, JSON.stringify(array));
        }
    }

    const showModal = (id, name) => {
        setModal({
            id: id,
            name: name,
            showModal: true
        });
    }

    const closeModal = () => {
        setModal({
            ...modal,
            showModal: false
        });
    }
    const closeCard = (id) => {
        let arrCart = [];
        let localCart = localStorage.getItem("cart");

        arrCart = JSON.parse(localCart);

        if (arrCart.includes(id)) {

            const index = arrCart.findIndex((element) => {
                return element === id;
            });
            arrCart.splice(index, 1);
            localStorage.setItem("cart", JSON.stringify(arrCart));

            setCartGoods({ cartGoods: arrCart })
        }
    }

    const { goods } = props;

    const buttonsAdd = [
        <Button key='1' text='OK' className='btn-ok button' onClickHandler={() => {
            saveToLocalCart(modal.id, modal.name);
        }} />,

        <Button key='2' text={"Cancel"} className="btn-cancel button" onClickHandler={closeModal} />
    ];

    const buttonsDel = [
        <Button key='1' text='OK' className='btn-ok button' onClickHandler={() => {
            closeCard(modal.id);
        }} />,

        <Button key='2' text={"Cancel"} className="btn-cancel button" onClickHandler={closeModal} />
    ];

    const modalInfoAdd = {
        title: "Add card",
        content: "Do you wanna add the product to cart?",
        closeButton: false
    }

    const modalInfoDel = {
        title: "Delete card",
        content: "Do you wanna delete the product to cart?",
        closeButton: false
    }
    const isSwitchModal = (bool) => {
        setSwitchModal({ switch: bool });
    }

    const arrayGoods = goods.map((good) => (
        <Card
            {...good}
            key={good.article}
            showModal={showModal}
            saveToLocalFavour={saveToLocalFavour}
            isClose={false}
            closeCard={() => closeCard(good.article)}
            isFavourite={favouriteGoods.favouriteGoods.includes(good.article)} />
    ))
    return (
        <>
            <Modal
                title={switchModal.switch ? modalInfoDel.title : modalInfoAdd.title}
                content={switchModal.switch ? modalInfoDel.content : modalInfoAdd.content}
                action={switchModal.switch ? buttonsDel : buttonsAdd}
                modalIsOpen={modal.showModal}
                closeModal={closeModal}
            />

            <Route path='/' component={Header} />
            <Route path='/' exact render={() => <div className='container d-flex justify-content-start flex-wrap cards-p' >{arrayGoods}</div>} />
            <Route path='/favourites' exact render={() => <Favourites goods={arrayGoods} favGoods={favouriteGoods.favouriteGoods} switch={isSwitchModal} />} />
            <Route path='/catch' exact render={() => <Catch goods={arrayGoods} catchGoods={cartGoods.cartGoods} closeCard={closeCard} switch={isSwitchModal} />} />




        </>
    );

}


export default CardList;