import React from "react";
import './header.scss';
import { NavLink } from 'react-router-dom';

export default () => {
    return (
        <div className="header">
            <nav className='nav'>
                <NavLink exact to='/' className='nav-link' activeClassName='active-link'>Home</NavLink>
                <NavLink  to='/favourites' className='nav-link' activeClassName='active-link'>Favourites</NavLink>
                <NavLink  to='/catch' className='nav-link' activeClassName='active-link'>Catch</NavLink>
            </nav>
        </div>
    )

}