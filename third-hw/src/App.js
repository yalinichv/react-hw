import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CardList from './containers/CardList/CardList'

import './app.scss'

function App() {

  const [data, setData] = useState({ goods: [] });

  useEffect(() => {
    axios('/goods.json')
      .then(result => {
        setData({ goods: result.data })
      })
  }, [])

  return (
    <div className="App">
      <div className="container">
        <div className='row'>
          <CardList goods={data.goods} />
        </div>
      </div>


    </div>
  )
}


export default App;
