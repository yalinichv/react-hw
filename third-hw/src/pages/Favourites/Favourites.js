import React, { useEffect } from 'react';

export default (props) => {
    useEffect(() => {
        props.switch(false);
    }, [])

    const { goods, favGoods } = props;
    const favCards = [];

    goods.forEach(good => {
        favGoods.includes(good.key) && favCards.push(good);
    })


    return (
        <div className="container">
            <div className='row'>
                {favCards}
                {console.log("Render Favourite")}

            </div>
        </div>
    )
}