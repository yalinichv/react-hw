import React, { useEffect } from 'react';



export default (props) => {

    useEffect(() => {
        props.switch(true);
    }, [])

    const { goods, catchGoods } = props;
    const catchCards = [];

    const func = (id) => {

        goods.forEach((good) => {
            if (good.props.article === id) {
                let newCard = {
                    ...good,
                    props: {
                        ...good.props,
                        isClose: true
                    }
                }
                catchCards.push(newCard)
            }
        });
    }

    catchGoods.forEach(id => {
        func(id);
    })

    return (
        <div className="container">
            <div className='row'>
                {catchCards}
            </div>
        </div>
    )
}