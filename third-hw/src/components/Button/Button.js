import React from 'react';
import "./button.scss";


function Button (props) {


        const { text, onClickHandler, className } = props;

        return (
            <button onClick={onClickHandler} className={className}>
                {text}
            </button>
        )

}

export default Button;