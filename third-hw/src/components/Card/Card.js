import React from "react";
import Button from "../Button/Button";
import './card.scss'
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";



function Card(props) {

    const { name, price, map, title, article, saveToLocalFavour, showModal, isFavourite, isClose } = props;

    const btn = {
        text: 'Add to catch',
        className: 'add-to-cart'
    }

    return (

        <div className="card-container">
            {isClose && <span className='close-card' onClick={() => {
                showModal(article, "cart");
            }}>X</span>}
            <img src={map} className="card-picture"></img>
            <h2 className="title-card">{name}
                <span className="icon-star">
                    <FontAwesomeIcon
                        icon={faStar}
                        className={isFavourite ? "favourite-on" : "favourite-off"}
                        onClick={(e) => {
                            e.preventDefault();
                            console.log("click")
                            saveToLocalFavour(article, "favourite");
                        }} />
                </span>

            </h2>
            <p className="describe">{title}</p>

            <div className='footer'>
                <span className="add-to-favour"></span>
                <span className="price-card">Price: {price}$</span>
                <Button text={btn.text} className={btn.className} onClickHandler={(e) => {
                    e.preventDefault();
                    showModal(article, "cart");
                }} />
            </div>
        </div>
    );

}

export default Card;