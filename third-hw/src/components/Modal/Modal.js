import React from 'react';
import './modal.scss';

function Modal(props) {

    const { title, content, action, modalIsOpen, closeModal } = props;

    return modalIsOpen && (

        <div className='shadow' onClick={closeModal}>
            <div className='window'>
                <header className='modal-header'>
                    <div className="title">{title}</div>
                    <div className='close-btn' onClick={closeModal}>X</div>
                </header>
                <div className='modal-b'>{content}</div>
                <div className='modal-controls'>{action}</div>
            </div>
        </div>
    )

}

export default Modal;