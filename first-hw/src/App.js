import React, { Component } from 'react';
import Button from "./components/Button"
import Modal from "./components/Modal";


class App extends Component {
  state = {
    buttonName: {
      buttonOne: 'Delete',
      buttonTwo: 'Copy'
    },
    modal1: {
      title: "Do you want to Delete this file?",
      content: "Once you Delete this file, it won’t be possible to undo this action.",
      closeButton: true
    },
    modal2: {
      title: "Do you want to Copy this file?",
      content: "Once you Copy this file, it won’t be possible to undo this action.",
      closeButton: true
    },
    modalData: {
      title: '',
      content: '',
      closeButton: true,
      showModal: false
    }
  }

  showModal = modalInfo => {

    const updateState = {
      modalData: {
        title: modalInfo.title,
        content: modalInfo.content,
        closeButton: modalInfo.closeButton,
        showModal: true
      }
    };

    this.setState(updateState);

  }

  closeModal = () => {

    const updateState = {
      modalData: {
        ...this.state.modalData,
        showModal: false
      }

    }
    debugger;
    this.setState(updateState);
  }



  render() {
    const { buttonName, modal1, modal2, modalData } = this.state;

    const buttons = [
      <Button key='1' text={"OK"} className='btn-ok' func={this.closeModal} />,
      <Button key='2' text={"Cancel"} className="btn-cancel " func={this.closeModal} />
    ];

    return (
      <>
        <Modal
          title={modalData.title}
          closeButton={modalData.closeButton}
          content={modalData.content}
          action={buttons}
          modalIsOpen={modalData.showModal}
          closeModal={this.closeModal} />

        <div className="main-section">

          <Button
            text={buttonName.buttonOne}
            func={this.showModal.bind(this, modal1)} />

          <Button
            text={buttonName.buttonTwo}
            func={this.showModal.bind(this, modal2)} />

        </div>
      </>
    )
  }
}

export default App;