import React from 'react';
import { Component } from 'react';


class Button extends Component {

    render() {
        const { text, func } = this.props;

        return (
            <button onClick={func} className="button">
                {text}
            </button>
        )
    }
}

export default Button;