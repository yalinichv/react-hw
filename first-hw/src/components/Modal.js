import React, { Component } from 'react';

class Modal extends Component {

    render() {
        const { title, closeButton, content, action, modalIsOpen, closeModal } = this.props;
        const closeModalBtn = (<div className='close-btn' onClick={closeModal}>X</div>)

        const isCloseModal = (e) => {
            if (e.target.classList.contains('shadow')) {
                closeModal();
            }
        }
        return modalIsOpen && (

            <div className='shadow' onClick={isCloseModal}>
                <div className='window'>
                    <header className='modal-header'>
                        <div className="title">{title}</div>
                        <div>{closeButton && closeModalBtn}</div>
                    </header>
                    <div className='modal-body'>{content}</div>
                    <div className='modal-controls'>{action}</div>
                </div>
            </div>
        )
    }
}

export default Modal;